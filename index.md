# ELK Stack - wprowadzenie

---

## AGENDA

* ELK Stack
* Beats
* Logstash
* ElasticSearch
* Kibana
* X-Pack
* Przykłady z podwórka
* World Use Cases

---

# <!-- .element class="fragment" -->Co to jest log?

---

# Po co nam logi?

---

## ELK Stack - pipeline

![](./assets/elk_stack.png)<!-- .element: style="background: white;" -->

---

![](./assets/elk-stack.svg)<!-- .element: style="background: white;" -->

---

## Beats

> Beats is the platform for single-purpose data shippers. They install as lightweight agents and send data from hundreds or thousands of machines to Logstash or Elasticsearch.

https://www.elastic.co/products/beats

---

![](./assets/beats.svg)<!-- .element: style="background: white;" -->

---

![](./assets/beats_kinds.png)

* Małe wyspecjalizowane serwisy instalowane na monitorowanych maszynach
* <!-- .element class="fragment" -->Napisane w Go
* <!-- .element class="fragment" -->Monitorują m. in. metryki maszyny, pliki, ruch w sieci, audyt, event logi windows, itd.
* <!-- .element class="fragment" -->Wysyłają dane do ElasticSearch, Logstash, Redis itd.

---

### Rodzaje beatów

* `FileBeat` - skanowanie plików
* `MetricBeat` - metryki maszyny
* `PacketBeat` - metryki połączenia sieciowego
* `WinlogBeat` - event logi Windows
* `AuditBeat` - śledzenie zmian w plikach
* `HeartBeat` - monitorowanie życia serwisów
* `ConsulBeat` - monitorowanie serwisów w Consul
* itd.

---

## Logstash

> Logstash is an open source, server-side data processing pipeline that ingests data from a multitude of sources simultaneously, transforms it, and then sends it to your favorite “stash.” (Ours is Elasticsearch, naturally.)

https://www.elastic.co/products/logstash

---

![](./assets/logstash-img1.png)<!-- .element: style="background: white;" -->

---

* Pipeline event processing
* <!-- .element class="fragment" -->Źródła: od plików, przez bazy danych, Redis, po porty i protokoły
* <!-- .element class="fragment" -->Filtruje, parsuje, przetwarza, taguje, zmienia
* <!-- .element class="fragment" -->Rozszerza dane wejsciowe (geo-IP)
* <!-- .element class="fragment" -->Zapisuje dane w dowolnym formacie (CSV, JSON, plik, ElasticSearch, inny logstash, itd.)

---

## ElasticSearch

> Elasticsearch is a distributed, RESTful search and analytics engine capable of solving a growing number of use cases. As the heart of the Elastic Stack, it centrally stores your data so you can discover the expected and uncover the unexpected.

https://www.elastic.co/products/elasticsearch

---

![](./assets/elasticsearch.svg)<!-- .element: style="background: white;" -->

---

* Rozproszona dokumentowa baza danych
* <!-- .element class="fragment" -->Wbudowane zapytania analityczne
* <!-- .element class="fragment" -->Dostęp do danych za pomocą REST API
* <!-- .element class="fragment" -->Oparta na Lucene
* <!-- .element class="fragment" -->Może pracować na petabajtach danych
* <!-- .element class="fragment" -->Jest szybka

---

## Kibana

> Kibana lets you visualize your Elasticsearch data and navigate the Elastic Stack, so you can do anything from learning why you're getting paged at 2:00 a.m. to understanding the impact rain might have on your quarterly numbers.

https://www.elastic.co/products/kibana

---

* Aplikacja WEB - napisana w node.js
* <!-- .element class="fragment" -->Interferjs użytkownika do dostępu do danych w ElasticSearch
* <!-- .element class="fragment" -->Wizualizuje dane w ElasticSearch
* <!-- .element class="fragment" -->Z dodatkiem X-Pack pozwala przewidywać awarie, informować o problemach
* <!-- .element class="fragment" -->Możliwość napisania własnych pluginów

---

## X-Pack

> On its own, the Elastic Stack is a force to be reckoned with. X-Pack takes it to a new level by bundling powerful features into a single pack.

https://www.elastic.co/products/x-pack

---

## Features

* Security
* <!-- .element class="fragment" -->Alerting
* <!-- .element class="fragment" -->Monitoring
* <!-- .element class="fragment" -->Reporting
* <!-- .element class="fragment" -->Graph
* <!-- .element class="fragment" -->Machine Learning

---

## Cena

---

## Przykłady z podwórka

---

* Mefisto (ofertownik) - analiza wykorzystania feature
* <!-- .element class="fragment" -->Rosa - monitorowanie operacji biznesowych + użycie RAM i CPU
* <!-- .element class="fragment" -->Frontend IIS - analiza i monitoring ruchu wchodzącego do KRD
* <!-- .element class="fragment" -->Backend PROD KRD - Monitoring zużycia RAM i CPU przez serwisy KRD
* <!-- .element class="fragment" -->JIRA - monitorowanie metryk serwera

---

# World Use cases

---

* Analiza ruchu przychodzącego - detekcja ataku, awarii sieci
* <!-- .element class="fragment" -->Analiza koszyka sprzedaży na stronie internetowej - Liczba transakcji zakonczonych sukcesem (https://www.youtube.com/watch?v=nJ7d65YSkVY)
* <!-- .element class="fragment" -->Analiza feature - użycie generuje przychód; porównanie z kosztem wytworzenia
* <!-- .element class="fragment" -->Analiza liczby i czasu realizacji transakcji kartami kredytowymi
* <!-- .element class="fragment" -->Analiza ruchu w sieciach GSM
* <!-- .element class="fragment" -->Analiza użytkowników w sieciach GSM

---

## Wirecard Tehnologies GmbH

> Our task is to take payments from e-commerce merchants, POS terminals and other channels, and deliver them to the networks of VISA, MasterCard, JCB and China UnionPay.

https://www.elastic.co/blog/how-wirecard-uses-the-elastic-stack-to-monitor-transactions-and-analyze-errors

---

> Our Kibana dashboard shows us:
> * Acceptance / rejection rates
> * Distribution to card-schemes (share of Visa, MasterCard, JCB, China UnionPay)
> * Distribution of the transactions on our geo-redundant data centers
> * Technical error codes
> * Percentile value of the transaction times

---

![](./assets/wirecard.png)

---

# Podsumowanie

---

![](./assets/elk-there-is-more-than-people-see.svg)

---

## Materiały

* https://www.elastic.co/
* https://logz.io/learn/complete-guide-elk-stack/
* https://www.youtube.com/watch?v=MRMgd6E9AXE
* https://www.youtube.com/watch?v=nJ7d65YSkVY

---

# Dziekuję